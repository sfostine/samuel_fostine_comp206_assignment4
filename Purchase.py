#!/usr/bin/python

#read the loggedIn.csv file
def loggedIn(name):
	# To the manager
	# add the right Path to the file LoggedIn.csv

    file_1 = open('LoggedIn.csv',"r")
    list_1 = []
     # read every logged in user, put them in a list then compare it with the username passing as parameter
    # from the hidden tag
    for line in file_1:
        for word in line.split(","):
            list_1.append(word.rstrip())
    file_1.close()
    if name in list_1:
        return True
    return False

# extract the information from the inventory file and put them in a list
def extractFromFile():
    file_2 = open('Inventory.csv',"r")	
    list_2 = []
    tra = 0
    for l in file_2:
        for w in l.split(","):
            # append the element from the file to a list
            # at the same time convert the price and the number of item to integer
            if tra%3 != 0:
                list_2.append(int(w.rstrip("\n")))
            else:
                list_2.append(w.rstrip("\n"))
            tra += 1
    file_2.close()
    return list_2
    
# do the inventory for each item
def inventory(lis, inst, rmqt): 
    # pass through the list to change the number in the inventory
    for i in range(len(lis)):
        if lis[i] == inst:
            # if there is not enough item in the inventory, return 1
            if lis[i+1] < rmqt:
                return 1
            else:
                lis[i+1] -= rmqt
        
    return 0

#write the modifies list in the inventory file again
def writeToInv(list_3):
    file_3 = open("Inventory.csv", 'w')
    for i in range(len(list_3)):
        if(i == 2 or i == 5 or i == 8 or i == 11): 
            file_3.write(str(list_3[i])+"\n")
        else:
            file_3.write(str(list_3[i]) + ",")
    file_3.close()
    
#import modules
import cgi, cgitb

#instance
form = cgi.FieldStorage()

# get the value from the hidden field
loggedInUser = form.getvalue("user")
t_price = 599.0 # trumpet price
cl_price = 449.0 # clarinet price
ce_price = 800.0 # cello price
d_price = 649.0 # drum price

t_q = int( form.getvalue("t_quantity")) # quantity of trumpet 
cl_q = int(form.getvalue("cl_quantity")) # quantity of clarinet
ce_q = int(form.getvalue("ce_quantity"))# quantity of cello
d_q = int(form.getvalue("d_quantity")) # quantity of drum  

print "Content-type:text/html\r\n\r\n"
print '<html>'
print '<head>'
print '<title>Checkbox</title>'
print '</head>'
print '<body bgcolor="#77B5FE">'

# variable for the number of item and the total cost
total = 0
numOfItem = 0
# create a list for the inventory
li = extractFromFile()
# if there is no logged in user
if loggedInUser == None or loggedIn(loggedInUser) == False:
	print '<h2>Error</h2>'
	print '<p>You are not currently logged in, please click on the CATALOGUE or HOME link below to go back to the catalogue or home page</p>'
	#print '<a link for home" target="_self" ><u>HOME | </u></a>'
	# TO THE MANAGER OF THE TEAM
	# Add the link to the HOME page here
	print '<a href="catalogue.html" target="_self"><u>CATALOGUE</u></a>'
# if there's a user logged in
# Store the billing information for each item
else:
	print '<center><h1>BILL</h1></center>'
	print '<center><h1>%d</h1></center>'%inventory(li,"trumpet",60)
	# if the box for trumpet was checked
	if form.getvalue("trumpet"):
		print '<img src="brass/trumpet.jpg" alt="Trumpet"style="width: 200px; height: 150px"/>'
		#check the inventory to see how many trumpets we have left
		# if inventory function returns 1 that means there's not enough in the inventory for the specific item
		if (inventory(li,"trumpet",t_q) == 1):
			print '<h3><font color="yellow"> We are so sorry, but we only have %d trumpet(s) in stock</font></h3>' %li[li.index("trumpet") + 1]
		else:
			price = t_q * t_price
			total += price
			numOfItem += t_q
               		print '<h3>Quantity of trumpet :%d</h3>' %(t_q)
                	print '<h3>$%.2f X %d = $%.2f</h3>' %(t_price, t_q, price)
              	print '<hr>'
		
	if form.getvalue("clarinet"):
		print '<td><img src="brass/clarinet.jpg" alt="Clarinet"style="width: 150px; height: 150px"></td>'
		if (inventory(li,"clarinet",cl_q) == 1):
                	print '<h3><font color="yellow"> We are so sorry, but we only have %d clarinet(s) in stock</font></h3>' %li[li.index("clarinet") + 1]
                else:	
			price = cl_q*cl_price
			total += price
			numOfItem += cl_q
                	print '<h3>Quantity of clarinet :%d</h3>' %(cl_q)
                	print '<h3>$%.2f X %d = $%.2f</h3>' %(cl_price, cl_q, price)
                print '<hr>'
	if form.getvalue("cello"):
		print '<td><img src="brass/cello.jpg" alt="Cello"style="width: 200px; height: 170px"></td>'
		if (inventory(li,"cello",ce_q) == 1):
                       print '<h3><font color="yellow"> We are so sorry, but we only have %d cello(s) in stock</font></h3>' %li[li.index("cello") + 1]
                else:		
			price = ce_q*ce_price
                	total += price
                	numOfItem += ce_q
                	print '<h3>Quantity of cello :%d</h3>' %(ce_q)
			print '<h3>$%.2f X %d = $%.2f</h3>' %(ce_price, ce_q, price)
                print '<hr>'
	if form.getvalue("drum"):
		print '<td><img src="brass/drum.jpg" alt="Drum"style="width: 200px; height: 170px"></td>'
		if (inventory(li,"drum",d_q) == 1):
                       print '<h3><font color="yellow"> We are so sorry, but we only have %d drum(s) in stock</font></h3>' %li[li.index("drum") + 1]
                else:
			price = d_q*d_price
                	total += price
               		numOfItem += d_q
                	print '<h3>Quantity of drum :%d</h3>' %(d_q)
                	print '<h3>$%.2f X %d = $%.2f</h3>' %(d_price, d_q, price)
                print '<hr>'

	# Update the inventory file
	writeToInv(li)
	print '<h1>Number of item in total : %d</h1>' %(numOfItem)
	print '<p><h1>Total price : $%.2f </h1></p>' %(total) 
 	print '<p><h3><font color="red">NB: If the number of item is not what you expected, please make sure that you check the box for each item you are purchasing</font></h3></p>'
	print '<h2><center>Thank you for purchasing at SamPhiMarc Music Store online</center></h2></br></br></br>'
	print 'You can go back to the catalogue or home page by clicking the CATALOGUE or HOME link below</br>'
	print '<a href="http://www.cs.mcgill.ca/~ptremb29/index.html" target="_self" ><u>HOME | </u></a>'
	print '<a href="http://www.cs.mcgill.ca/~ptremb29/catalogue.html" target="_self" ><u>CATALOGUE</u></a>'
print '</body>'
print '</html>'
