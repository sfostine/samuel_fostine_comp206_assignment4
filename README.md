# README #

For glory, we update the inventory after each purchase, so when there is not enough items in the inventory, vi the Inventory.csv file, change the quantity of item. The Inventory.csv file stores values like this:
item1,quantity,price
item2,quantity,price
item3,quantity,price

so you will just have to change the value of quantity for the item

### What is this repository for? ###

Python script for the catalogue page of the website with the html file and the Inventory. The brass folder contains pictures about the items

### Contribution guidelines ###

The files will be put together as a project with other teams members: Phillippe and Marc
Thanks to them

### Who do I talk to? ###

* Samuel Fostine, McGill ID: 260586688